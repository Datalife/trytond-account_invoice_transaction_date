# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    def _get_invoice_purchase(self):
        invoice = super()._get_invoice_purchase()
        if invoice:
            invoice.transaction_date = self._get_transaction_date()
            invoice.on_change_transaction_date()
        return invoice

    def _get_transaction_date(self):
        if self.moves:
            dates = [m.effective_date or m.planned_date
                for m in self.moves if (m.effective_date or m.planned_date)]
            if dates:
                return max(dates)
        return self.purchase_date


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @property
    def transaction_date(self):
        if self.purchase:
            return self.purchase._get_transaction_date()
