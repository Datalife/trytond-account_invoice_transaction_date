# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Eval, If

_DATE_FMT = '%d-%m-%Y'


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    transaction_date = fields.Date('Transaction Date',
        domain=[
            If(Eval('state').in_(['draft', 'validated']), [
                If(Eval('transaction_date') & Eval('invoice_date'),
                    ('transaction_date', '<=', Eval('invoice_date')),
                    ()
                ),
                If(Eval('transaction_date') & Eval('accounting_date'),
                    ('transaction_date', '<=', Eval('accounting_date')),
                    ()
                )],
            [])
        ],
        states={
            'readonly': Eval('state').in_(['posted', 'paid', 'cancelled']),
        }, depends=['state', 'invoice_date', 'accounting_date'])

    @fields.depends('type', 'transaction_date', 'accounting_date')
    def on_change_transaction_date(self):
        if self.type == 'out' and self.transaction_date and \
                not self.accounting_date:
            self.accounting_date = self.transaction_date

    @fields.depends(methods=['compute_transaction_date'])
    def on_change_lines(self):
        super().on_change_lines()
        self.compute_transaction_date()

    @fields.depends('lines', methods=['on_change_transaction_date'])
    def compute_transaction_date(self):
        dates = set()
        for line in self.lines:
            date = line._compute_transaction_date()
            if date:
                dates.add(date)
        if dates:
            self.transaction_date = max(dates)
            self.on_change_transaction_date()


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def _compute_transaction_date(self):
        if self.stock_moves:
            dates = set(m.effective_date or m.planned_date
                for m in self.stock_moves if (m.effective_date or
                    m.planned_date))
            if dates:
                return max(dates)
        elif self.origin:
            # avoid to raise exception if field does not exists on origin
            return getattr(self.origin, 'transaction_date', None)


class Invoice2(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    def check_same_dates(self):
        # disable check
        pass


class InvoiceSII(metaclass=PoolMeta):
    __name__ = 'account.invoice.sii'

    def get_invoice_detail(self):
        detail = super().get_invoice_detail()
        detail['FechaOperacion'] = self.invoice.transaction_date.strftime(
            _DATE_FMT) if self.invoice.transaction_date else None
        return detail
